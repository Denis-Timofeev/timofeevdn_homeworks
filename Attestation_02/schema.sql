
CREATE TABLE product
(
    id SERIAL PRIMARY KEY,
    description VARCHAR(100),
    price DOUBLE PRECISION CHECK (price > 0 and price < 10000),
    amount INTEGER
);

INSERT INTO product (description, price, amount)
VALUES ('Textile - Footer "White"', 990, 100);
INSERT INTO product (description, price, amount)
VALUES ('Textile - Flax "Black"', 1100, 90);
INSERT INTO product (description, price, amount)
VALUES ('Textile - Footer "Blue"', 940, 110);
INSERT INTO product (description, price, amount)
VALUES ('Textile - Footer "Yellow"', 920, 110);

SELECT * FROM product WHERE price > 1000;

SELECT amount, description FROM product WHERE amount < 100;

SELECT COUNT(*) FROM product;

CREATE TABLE customers
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(50),
    surname VARCHAR(150)
);

INSERT INTO customers(name, surname)
VALUES ('Максим', 'Петров');
INSERT INTO customers(name, surname)
VALUES ('Владимир', 'Сидоров');
INSERT INTO customers(name, surname)
VALUES ('Яна', 'Иванова');
INSERT INTO customers(name, surname)
VALUES ('Ирина', 'Морозова');

SELECT COUNT(*) FROM customers;

SELECT surname FROM customers;

SELECT name FROM customers;

CREATE TABLE orders
(
    id SERIAL PRIMARY KEY,
    product_id INTEGER,
    FOREIGN KEY (product_id) REFERENCES product (id),
    customer_id INTEGER,
    FOREIGN KEY (customer_id) REFERENCES customers (id),
    Date DATE,
    number_of_goods INTEGER CHECK (number_of_goods > 0)
);

INSERT INTO orders (product_id, customer_id, Date, number_of_goods) VALUES (1,1, now(), 5);
INSERT INTO orders (product_id, customer_id, Date, number_of_goods) VALUES (2,2, now(), 1);
INSERT INTO orders (product_id, customer_id, Date, number_of_goods) VALUES (3,3, now(), 10);
INSERT INTO orders (product_id, customer_id, Date, number_of_goods) VALUES (4,4, now(), 50);

SELECT surname, (SELECT COUNT(*) FROM orders WHERE customer_id = 1) AS orders_count
FROM customers
WHERE id = 3;

SELECT COUNT(*) FROM orders;

SELECT name, surname (SELECT COUNT(*) FROM orders WHERE number_of_goods = 50);


package src_10;

public class Main {
    public static void main(String[] args) {
        Move[] figures = new Move[2];
        Square square = new Square(10, 5);
        Circle circle = new Circle(0, 15);
        square.getMoveXY(5, 3);
        circle.getMoveXY(7, 7);
        figures[0] = square;
        figures[1] = circle;
        for (Move figure : figures) {
            figure.getMoveCoordinates(0, 0);
        }
    }
}
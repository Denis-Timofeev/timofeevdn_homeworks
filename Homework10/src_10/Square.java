package src_10;

public class Square extends Rectangle implements Move {

    public Square(int x, int y) {
        super(x, y);
    }

    public void getMoveXY(int value3, int value4) {
        this.x += value3;
        this.y += value4;
        System.out.println("Square coords: " + " " + this.getX() + " " + this.getY() + " ");
    }

    public void getMoveCoordinates(int value1, int value2) {
        this.x = value1;
        this.y = value2;
        System.out.println("Square coords: " + " " + this.getX() + " " + this.getY() + " ");
    }
}
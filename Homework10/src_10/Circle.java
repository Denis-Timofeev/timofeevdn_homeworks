package src_10;

public class Circle extends Ellipse implements Move {

    public Circle(int x, int y) {
        super(x, y);
    }

    public void getMoveXY(int value3, int value4) {
        this.x += value3;
        this.y += value4;
        System.out.println("Circle coords: " + " " + this.getX() + " " + this.getY() + " ");
    }

    public void getMoveCoordinates(int value1, int value2) {
        this.x = value1;
        this.y = value2;
        System.out.println("Circle coords: " + " " + this.getX() + " " + this.getY() + " ");
    }
}
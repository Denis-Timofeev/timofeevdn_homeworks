package src_10;

public abstract class Figure {
    protected int x;
    protected int y;
    protected Move move;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
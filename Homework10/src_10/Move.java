package src_10;

public interface Move {
    void getMoveCoordinates(int value1, int value2);
    void getMoveXY(int value3, int value4);
}
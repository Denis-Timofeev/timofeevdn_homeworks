///////
import java.util.List;

public interface UsersRepository {
    User findById(int id);
    void update(User user);
}

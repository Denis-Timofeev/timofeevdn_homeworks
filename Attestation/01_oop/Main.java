//////
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        User user = usersRepository.findById(3);

        List<User> users = new ArrayList<>();
        users.add(user);
        for (User v : users) {
            System.out.println(v.getAge() + " " + v.getName() + " " + v.isWorker());
        }

        user.setAge(35);
        usersRepository.update(user);
    }
}

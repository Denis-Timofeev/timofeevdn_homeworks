package src_09;
class Ellipse {
    protected int r1;
    private int r2;

    public Ellipse(int x, int y, int r1, int r2) {
        this.r1 = r1;
        this.r2 = r2;
    }

    public double getPerimeter() {
        return 2 * Math.PI * Math.sqrt((r1 * r1 + r2 * r2) / 2.);
    }
}
package src_09;

class Rectangle {
    protected int a;
    private int b;

    public Rectangle(int x, int y, int a, int b) {
        this.a = a;
        this.b = b;
    }

    public double getPerimeter() {
        return (a + b) * 2;
    }
}

package src_09;

class Square extends Rectangle {
    public Square(int x, int y, int a) {
        super(x, y, a, a);
    }

    public double getPerimeter() {
        return a * 4;
    }
}

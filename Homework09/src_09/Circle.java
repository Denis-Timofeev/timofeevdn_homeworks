package src_09;
class Circle extends Ellipse {
    public Circle(int x, int y, int r1) {
        super(x, y, r1, r1);
    }

    public double getPerimeter() {
        return 2 * Math.PI * r1;
    }
}

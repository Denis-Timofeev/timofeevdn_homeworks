package src_09;
public class Program09 {
        public static void main(String[] args) {
            Figure figure = new Figure(1, 2);
            Square square = new Square(0, 9, 7);
            Circle circle = new Circle(9, 1, 4);
            Ellipse ellipse = new Ellipse(1, 1, 5, 7);
            Rectangle rectangle = new Rectangle(0, 0, 5, 10);
            System.out.println("Figure: " + " " + figure.getPerimeter());
            System.out.println("Square: " + " " + square.getPerimeter());
            System.out.println("Ellipse: " + " " + ellipse.getPerimeter());
            System.out.println("Circle: " + " " + circle.getPerimeter());
            System.out.println("Rectangle: " + " " + rectangle.getPerimeter());
        }
    }

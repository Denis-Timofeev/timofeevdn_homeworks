import java.util.Scanner;
public class Program07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] arr = new int[200];
        int a = scanner.nextInt();
        while (a != -1) {
            arr[a + 100]++;
            a = scanner.nextInt();
        }
        int min = 2147483647;
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > 1 && min > arr[i]) {
                min = arr[i];
                index = i;
            }
        }
        System.out.println(index - 100);
    }
}

